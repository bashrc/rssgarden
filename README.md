RSS Garden
==========

<img src="https://code.freedombone.net/bashrc/rssgarden/raw/master/rssgarden.png?raw=true" width="10%"/>

Show RSS feeds in your motd or on a web page.

Installation
------------

On Debian/Ubuntu:

``` bash
sudo apt-get install python3-pip
```

Install dependencies:

``` bash
pip3 install atoma
```

Install the application:

``` bash
sudo make install
```

Usage
-----

Create a file called *~/.feeds* and add RSS feed URLs to it.

Initialize for the first time:

``` bash
sudo update-feeds
```

Reload your terminal.

``` bash
. ~/.bashrc
```

You should then see the last few feed links.

On a web page
-------------

<img src="https://code.freedombone.net/bashrc/rssgarden/raw/master/web.png?raw=true" width="90%"/>

If you want to turn the feed list for a given user into a html page to appear on a website then edit /etc/rssgarden/config.txt. It should have a format like:

``` bash
username=bob
web=/var/www/html
web_header=Title of my page
```

Here *web* indicates the path for the web server and *username* is the user whose feeds we want to display. Feeds can then be read at /var/www/html/feeds.html

In the above example a style sheet will also be saved to /var/www/html/feeds.css. You can edit this to change the style as preferred.

<img src="https://code.freedombone.net/bashrc/rssgarden/raw/master/web_edit.png?raw=true" width="50%"/>

If you want to add or remove feeds click on the title and then enter an RSS feed URL. When removing feeds you can just enter the domain name rather than the full URL.
