#!/usr/bin/python3
#
# Copyright (C) 2019 Bob Mottram <bob@freedombone.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import atoma
import requests
import time
from subprocess import check_output
import os, sys
import os.path
import pwd
import string
import shutil
import subprocess

posts = []

def read_rss(url):
    feed_parsed = False

    if os.path.isfile('/usr/bin/torsocks'):
        # Obtain feed via tor
        rss_feed_download = '/tmp/rssgarden.xml'
        if os.path.isfile(rss_feed_download):
            os.remove(rss_feed_download)
        commandstr='/usr/bin/torsocks wget ' + url + ' -O ' + rss_feed_download
        subprocess.run(commandstr, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8')
        if os.path.isfile(rss_feed_download):
            try:
                feed = atoma.parse_rss_file(rss_feed_download)
                os.remove(rss_feed_download)
                feed_parsed = True
            except Exception:
                pass
        else:
            print('Command failed: ' + commandstr)
    else:
        try:
            response = requests.get(url)
            feed = atoma.parse_rss_bytes(response.content)
            feed_parsed = True
        except Exception:
            pass

    if feed_parsed:
        for post in feed.items:
            try:
                if post.pub_date and post.title and post.link:
                    posts.append([ post.pub_date, post.title, post.link ])
            except Exception:
                pass

def read_feeds_file(filename):
    with open(filename, 'r') as feeds_list:
        for line in feeds_list:
            linestr = line.strip()
            if linestr.startswith('http'):
                read_rss(linestr)
        feeds_list.close()

def read_config_param(config_filename, param_name):
    if os.path.isfile(config_filename):
        with open(config_filename, 'r') as config_list:
            for line in config_list:
                if line.startswith(param_name + '='):
                    config_list.close()
                    return line.split('=',1)[1].strip()
    return None

def process_feed_edits(web_path, username):
    if web_path and username:
        feed_edits_filename = web_path + '/.feeds_edits.txt'
        feeds_filename = '/home/' + username + '/.feeds'
        if os.path.isfile(feed_edits_filename):
            with open(feed_edits_filename, 'r') as feeds_edits:
                for line in feeds_edits:

                    if line.startswith('+'):
                        rss_feed_url = line[1:]
                        already_exists = False
                        if os.path.isfile(feeds_filename):
                            with open(feeds_filename, 'r') as user_feeds:
                                if rss_feed_url in user_feeds.read():
                                    already_exists = True
                                user_feeds.close()
                        if not already_exists:
                            with open(feeds_filename, 'a') as user_feeds:
                                user_feeds.write(rss_feed_url + '\n')
                                user_feeds.close()

                    if line.startswith('-'):
                        if os.path.isfile(feeds_filename):
                            rss_feed_url = line[1:]

                            output = []
                            line_removed = False
                            with open(feeds_filename, 'r') as user_feeds:
                                for line in user_feeds:
                                    if not rss_feed_url in line:
                                        output.append(line)
                                    else:
                                        line_removed = True

                                user_feeds.close()
                            if line_removed:
                                with open(feeds_filename, 'w') as user_feeds:
                                    for line in output:
                                        user_feeds.write(line)
                                user_feeds.close()
                                uid = pwd.getpwnam(username).pw_uid
                                os.chown(feeds_filename, uid, uid)

                feeds_edits.close()
            os.remove(feed_edits_filename)

def update_user_feeds(username, motd_lines, web_path, web_path_read_only, web_header_text, web_title_text):
    uid = pwd.getpwnam('www-data').pw_uid
    html_filename = ''
    if web_path:
        html_filename = web_path + '/feeds.html'
        html_filename_read_only = web_path_read_only + '/feeds.html'
        if not os.path.isfile(web_path + '/feeds.css'):
            shutil.copy('/etc/rssgarden/feeds.css', web_path + '/feeds.css')
            os.chown(web_path + '/feeds.css', uid, uid)
        shutil.copy('/etc/rssgarden/feeds_edit.html', web_path + '/feeds_edit.html')
        os.chown(web_path + '/feeds_edit.html', uid, uid)
        shutil.copy('/etc/rssgarden/rssgarden.php', web_path + '/rssgarden.php')
        os.chown(web_path + '/rssgarden.php', uid, uid)

    feeds_filename = '/home/' + username + '/feeds'
    if os.path.isfile('/home/' + username + '/.feeds'):
        feeds_filename = '/home/' + username + '/.feeds'

    feeds_motd = '/home/' + username + '/.feeds_motd'

    bashrc_file = '/home/' + username + '/.bashrc'
    found_in_bashrc = False
    if os.path.isfile(bashrc_file):
        file_t = open(bashrc_file, 'r')
        file_text = file_t.read()
        if 'feeds_motd' in file_text:
            found_in_bashrc = True
        if 'controluser' in file_text:
            found_in_bashrc = True
        file_t.close()
        if not found_in_bashrc:
            f = open(bashrc_file, 'a')
            f.write("if [ -f ~/.feeds ]; then\n")
            f.write("    if [ -f ~/.feeds_motd ]; then\n")
            f.write("        cat ~/.feeds_motd\n")
            f.write("    fi\n")
            f.write("fi\n")
            f.close()

    f = open(feeds_motd, 'w')

    if os.path.isfile(feeds_filename):
        read_feeds_file(feeds_filename)

        sorted_posts = sorted(posts, reverse=True)

        if html_filename:
            htmlfile = open(html_filename, 'w')
            htmlfile.write("<html>\n")
            htmlfile.write("<meta http-equiv=\"Refresh\" content=\"600\" charset=\"utf-8\">")

            htmlfile.write("<style>\n")
            htmlfile.write("@import url(\"feeds.css\");\n")
            htmlfile.write("    .row {\n")
            htmlfile.write("        display: -ms-flexbox;\n")
            htmlfile.write("        display: flex;\n")
            htmlfile.write("        -ms-flex-wrap: wrap;\n")
            htmlfile.write("        flex-wrap: wrap;\n")
            htmlfile.write("        padding: 0 4px;\n")
            htmlfile.write("    }\n\n")
            htmlfile.write("    .column {\n")
            htmlfile.write("        -ms-flex: 100%;\n")
            htmlfile.write("        flex: 100%;\n")
            htmlfile.write("        max-width: 80%;\n")
            htmlfile.write("        padding: 0 10%;\n")
            htmlfile.write("    }\n")
            htmlfile.write("</style>\n")
            htmlfile.write("<body><br>\n")

            if os.path.isfile(web_path + '/images/logo.png'):
                htmlfile.write("<center><a href=\"index.html\" title=\"Go Back\"><img id=\"headerpic\" class=\"img-responsive\" src=\"images/logo.png\" alt=\"Go Back\"></a></center>\n")

            if web_header_text:
                htmlfile.write("<h4 class=\"subheader\">" + web_header_text + "</h4>\n")
            if web_title_text:
                htmlfile.write("<h1 class=\"header\"><a href=\"feeds_edit.html\">RSS " + web_title_text + "</h1>\n")
            else:
                htmlfile.write("<h1 class=\"header\"><a href=\"feeds_edit.html\">RSS <img src=\"images/add_app.png\" width=\"50px\" style=\"vertical-align: middle\" title=\"Add or remove feeds\"/></a></h1>\n")

        ctr = 0
        prev_headline = ''
        prev_motd = ''
        for entry in sorted_posts:
            published_time = entry[0]

            increment_counter = False
            if ctr < motd_lines:
                if entry[1] != prev_motd:
                    f.write(str(published_time) + ', ' + entry[1] + ', ' + entry[2] + "\n")
                    prev_motd = entry[1]
                    increment_counter = True
            else:
                if not html_filename:
                    break

            if html_filename:
                if entry[1] != prev_headline:
                    htmlfile.write("<div class=\"row\">\n")
                    htmlfile.write("<div class=\"column\">\n")
                    htmlfile.write("<div>\n")
                    htmlfile.write("<p class=\"feeds\"><a href=\"" + entry[2] + '">' + entry[1] + '</a></p>' + "\n")
                    htmlfile.write("</div>\n")
                    htmlfile.write("</div>\n")
                    htmlfile.write("</div>\n")
                    prev_headline = entry[1]
                    increment_counter = True

            if increment_counter:
                ctr = ctr + 1

            if ctr == 64:
                break

        if html_filename:
            htmlfile.write("</body>\n")
            htmlfile.write("</html>\n")
            htmlfile.close()
            os.chown(html_filename, uid, uid)
            if web_path_read_only:
                temp_feeds_html='/tmp/feeds.html'
                shutil.copy(html_filename, temp_feeds_html)
                with open(temp_feeds_html, "r") as fin:
                    with open(html_filename_read_only, "w") as fout:
                        for line in fin:
                            fout.write(line.replace('feeds_edit.html', 'index.html'))
                        fout.close()
                    fin.close()
                os.remove(temp_feeds_html)

        f.close()

        # set permissions on the motd file
        uid = pwd.getpwnam(username).pw_uid
        os.chown(feeds_motd, uid, uid)

def update_cron():
    if os.path.isfile('/etc/crontab'):
        already_exists = False
        with open('/etc/crontab', 'r') as crontab:
            if 'rssgarden' in crontab.read():
                already_exists = True
            crontab.close()

        if not already_exists:
            with open('/etc/crontab', 'a') as crontab:
                crontab.write("*/5 * * * * root /etc/rssgarden/cron_update\n")
                crontab.close()

def update_all_users(motd_lines):
    html_username = read_config_param('/etc/rssgarden/config.txt', 'username')
    web_path = read_config_param('/etc/rssgarden/config.txt', 'web')
    web_path_read_only = read_config_param('/etc/rssgarden/config.txt', 'web_read_only')
    web_header_text = read_config_param('/etc/rssgarden/config.txt', 'web_header')
    web_title_text = read_config_param('/etc/rssgarden/config.txt', 'web_title')

    # Check that the web directory exists
    if web_path:
        if not os.path.isdir(web_path):
            web_path = None

    update_cron()
    process_feed_edits(web_path, html_username)

    checked_directories = []
    for subdir, dirs, files in os.walk('/home'):
        for directory in dirs:
            if directory not in checked_directories:
                if os.path.isdir('/home/' + directory):
                    checked_directories.append(directory)
                    if web_path and directory == html_username:
                        update_user_feeds(directory, motd_lines, web_path, web_path_read_only, web_header_text, web_title_text)
                    else:
                        update_user_feeds(directory, motd_lines, '', '', '', '')

update_all_users(3)
