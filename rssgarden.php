<?php

// Copyright (C) 2019 Bob Mottram <bob@freedombone.net>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

$output_filename = "feeds.html";

if (php_sapi_name()!=='fpm-fcgi') exit('php script must be run from the web interface');

function filter_string($var, $maxlen=256)
{
    if (isset($_POST[$var])) {
        if (strlen($_POST[$var]) < $maxlen) {
            return true;
        }
    }
    return false;
}

if (isset($_POST['submitadd'])) {
    if(filter_string('rss_feed_url')) {
        $rss_feed_url = htmlspecialchars($_POST['rss_feed_url']);

        if(strlen($rss_feed_url) > 4) {
            if(substr($rss_feed_url, 0, 4) === "http") {
                $feeds_file = fopen(".feeds_edits.txt", "a") or die("Unable to write to .feeds_edits.txt");
                fwrite($feeds_file, '+'.$rss_feed_url);
                fclose($feeds_file);
            }
        }
    }
}

if (isset($_POST['submitremove'])) {
    if(filter_string('rss_feed_url')) {
        $rss_feed_url = htmlspecialchars($_POST['rss_feed_url']);

        if(strlen($rss_feed_url) > 4) {
            $feeds_file = fopen(".feeds_edits.txt", "a") or die("Unable to write to .feeds_edits.txt");
            fwrite($feeds_file, '-'.$rss_feed_url);
            fclose($feeds_file);
        }
    }
}

if (isset($_POST['submitcancelinitial'])) {
    $output_filename = "index.html";
}

$htmlfile = fopen("$output_filename", "r") or die("Unable to open $output_filename");
echo fread($htmlfile,filesize("$output_filename"));
fclose($htmlfile);

?>
