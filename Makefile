APP=rssgarden

all:
debug:
install:
	mkdir -p ${DESTDIR}/etc/${APP}
	cp update-feeds ${DESTDIR}/usr/bin
	cp ${APP}.py ${DESTDIR}/etc/${APP}
	cp cron_update ${DESTDIR}/etc/${APP}
	cp feeds_edit.html ${DESTDIR}/etc/${APP}
	cp ${APP}.php ${DESTDIR}/etc/${APP}
	cp feeds.css ${DESTDIR}/etc/${APP}
	cp ${APP}.png ${DESTDIR}/etc/${APP}
	cp -r images ${DESTDIR}/etc/${APP}
	cp cron ${DESTDIR}/etc/cron.daily/${APP}
uninstall:
	rm -f /etc/cron.daily/${APP}
	rm -f /usr/bin/update-feeds
	rm -rf /etc/${APP}
